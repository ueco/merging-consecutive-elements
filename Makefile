merging_consecutive_elements: 
	g++ -std=c++17 -O2 -Wall -pedantic -pthread src/algorithm.cpp -o algorithm

clean:
	rm algorithm
